"""
BAD GAUSS

A simple plot of a gaussian PDF ... the hard way!

"""


import math
import matplotlib.pyplot as plt

def bad_gauss(x,mu=0.0,sigma=1.0):
	
	f = [] # create array

	# loop through x values, finding probability at each point
	for xx in x:
		ff = math.exp(-.5*((xx-mu)/sigma)**2)/math.sqrt(2.0*math.pi*sigma**2)
		f.append(ff)
	
	return f


# create an array of x-values uniformly spaced from xmin to xmax
xMin = -5.0
xMax = 5.0
Nx = 100

dx = (xMax - xMin) / ( Nx - 1 )

x = []
for i in range(Nx):
	x.append( xMin + i * dx )


# compute gaussian probability
f = bad_gauss(x)


# check the moments ...
sum,mean,msquare = 0.0,0.0,0.0
for ff,xx in zip(f,x):
	sum += ff*dx
	mean += ff*xx*dx
	msquare += ff*xx*xx*dx


print "integral of PDF: " , sum
print "mean:", mean
print "variance:",msquare - mean**2

# plot it
plt.plot( x , f, 'k-',linewidth=2)
plt.show()


