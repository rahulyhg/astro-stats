"""
GOOD GUASS

Plots a simple gaussian.

"""

import numpy as np
import matplotlib.pyplot as plt

def good_gauss(x,mu=0.0,sigma=1.0):
	return np.exp(-.5*((x-mu)/sigma)**2)/(sigma*np.sqrt(2.0*np.pi))

# compute gaussian probabiltiy
mu,sigma = 5.0,3.0
x = np.linspace(-5.0*sigma,5.0*sigma,100) + mu
f = good_gauss(x,sigma=sigma,mu=mu)

# check moments ...
dx = (x[1]-x[0])
sum = np.sum(f) * dx
mean = np.dot(x,f) * dx
msqr = np.dot(x**2,f) * dx

print "Sum / Mean / Var : %.2f / %.2e / %.2f " \
	% ( sum , mean, msqr - mean**2 )

plt.plot(x,f,'k-',linewidth=2)
plt.show()
