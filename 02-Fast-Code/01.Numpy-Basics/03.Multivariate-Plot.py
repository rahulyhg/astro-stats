"""
MULTIVARIATE PLOT

Plots a 2D profile of gas density for a disk
galaxy ...
"""

import numpy as np
import matplotlib.pyplot as plt
import galaxy as gal
import cgs


# setup a 2D mesh of points in cylindrical radius
# and height above plane ...
r = np.linspace(-5.0,5.0,100) * cgs.kpc
z = np.linspace(-5.0,5.0,100) * cgs.kpc
R,Z = np.meshgrid(r,z)


# compute a 2D cutaway of ISM gas density for an
# LMC-like galaxy
rho = gal.gas_disk_density(R,Z)


# plot the result as an image 
extent = ( r[0] , r[-1] , z[0], z[-1] )
plt.imshow(rho/cgs.mp,extent=extent)
plt.colorbar()
plt.xlabel('radius [ kpc ]')
plt.ylabel('height [ kpc ]')
plt.title('Gas Density ( proton masses / cubic cm )')
plt.show()
