"""
RANDOM SAMPLING

Samples from a 2D joint normal distribution
with set mean vector and covariance matrix,
takes a cut from this data, and bins a scalar
measure of distance.
"""

import numpy as np
import matplotlib.pyplot as plt

N_TRIALS = 1000

# define mean and covariance matrix for 
# 2D joint normal distribution ...
mean = np.array([0.5,0.5])
sx,sy,rho = 1.0,1.0,0.5
cov = np.array([[sx**2,rho*sx*sy],[rho*sx*sy,sy**2]])

# given mean, covariance, sample from the joint normal
# distribution ...
x,y = np.random.multivariate_normal(mean,cov,N_TRIALS).T

# Make a cut of this data:
# 	example: X,Y > 0.0
cut = np.logical_and( x-y < 0.5 , x-y > -0.5 )
x_cut, y_cut = x[cut], y[cut]

# Create derived quantitiy from sampling ... perhaps
# the distance square
def d2(x,y):
	return x**2 + y**2

# Plot results
fig, ((ax1),(ax2)) = plt.subplots(2,1)

ax1.scatter(x,y,marker='*',color='grey')
ax1.scatter(x_cut,y_cut,marker='*',color='b')

ax2.hist(d2(x,y),50,facecolor='grey',edgecolor='white')
ax2.hist(d2(x_cut,y_cut),50,facecolor='b',edgecolor='white')

# add some labels:
plots = { ax1 : 'Sample Points' , ax2 : 'Distance Distribution' }

for ax,title in plots.items():
	ax.set_title(title)

plt.show()
