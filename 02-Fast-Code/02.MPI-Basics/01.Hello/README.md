01.16.Hello
-----------

Basic 'hello world' for mpi.

- spins up processors, root processors pings each
  processor by sending a message, waits.
- Other processors recieve message, print, and then
  respond
- root collects responses and loops through
  remaining processors, then exits.

**To Compile:** mpicc -o Hello.exe hello.C

**To Run:** mpirun -np 5 Hello.exe

