01.16.Riemann
=============

Estimates integral of 4/(1+x^2) from 0 to 1 which should give
pi, using simple rectangles. The sum is divied evenly among
processors, using MPI_Bcast and MPI_Reduce commands. The divying
is accomplished with the for-loop:

		for( size_t i = myid ; i < n ; i += numprocs )

so each processor starts at a different cell, and skips over
the other processors on the next iteration. MPI_Reduce then collects
the info, and sums it together.

Timing Results
--------------

We used the MPI_Wtime() command to asses performance on a run with 10^10
rectangles. This was run on a TACC Stampede login node.

- **Single Processor:** 11 seconds (best)
- **Ten Processors:**   1.5 +/- .4 seconds

In one go, the single processor gave 25.4 +/- .8 seconds run time, which 
seemed abysmal. In another repitition, it did 11 seconds. So it seems
quite variable on the login node. The multi-processor version was much
more consistent.

------------------

**To Compile:** mpicc -o Riemann.exe Riemann.C

**To Run:** mpirun -np 5 Riemann.exe

