01.16.MatrixMult
================

Simple program to perform inner product between a matrix, 'a' and
a vector, 'b', using "master/slave" approach and sending MPI_Send
and MPI_Recv messages. This was translated from fortran.

------------------

**To Compile:** mpicc -o Matrix.exe matrix.C

**To Run:** mpirun -np 5 Matrix.exe
