Chapter 2
---------

Fast Computation on Massive Datasets
====================================


Resources for all the code you find here can
be found in the following places:

- For Numpy and Scipy visit:
	- http://docs.scipy.org/doc/


- For MPI and more on parallel programming,
	check out the book "Using Mpi: Portable 
	Parallel Programming with the Message 
	Passing Interface", available through
	- http://clio.columbia.edu


- For CUDA and more on GPUs check out the
	book "CUDA Programming: A Developer's Guide 
	to Parallel Computing with GPUs", available
	free through Columbia Library:
	- http://clio.columbia.edu
